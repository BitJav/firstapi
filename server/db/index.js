const Sequelize = require('sequelize');
const UserModel = require('./models/user');
const ProductModel = require('./models/product');
const CityModel = require('./models/city');
const StateModel = require('./models/state');

const env = process.env.NODE_ENV || 'development';
const config = require('./config/config.json')[env];

const { database, user, password, ...options } = config;

const sequelize = new Sequelize(database, user, password, options);

const User = UserModel(sequelize, Sequelize);
const Product = ProductModel(sequelize, Sequelize);
const City = CityModel(sequelize, Sequelize);
const State = StateModel(sequelize, Sequelize);

City.hasMany(User);
State.hasMany(User);
User.belongsTo(City);
User.belongsTo(State);

module.exports = { User, Product, City, State };
