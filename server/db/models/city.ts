import {
  Model,
  DataTypes,
  Optional,
  HasManyGetAssociationsMixin,
  HasManyAddAssociationMixin,
  HasManyHasAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  Association,
} from 'sequelize';
import { sequelize } from './index';
import User from './user';

export interface CityAttributes {
  id: number;
  name: string;
}

// Some attributes are optional in `City.build` and `City.create` calls
export interface CityCreationAttributes
  extends Optional<CityAttributes, 'id'> {}

class City
  extends Model<CityAttributes, CityCreationAttributes>
  implements CityAttributes
{
  declare id: number;
  declare name: string;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;

  declare getCities: HasManyGetAssociationsMixin<City>;
  declare addCity: HasManyAddAssociationMixin<City, number>;
  declare hasCity: HasManyHasAssociationMixin<City, number>;
  declare countCities: HasManyCountAssociationsMixin;
  declare createCity: HasManyCreateAssociationMixin<City>;

  declare static associations: {
    users: Association<City, User>;
  };
}

City.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
  },
  {
    sequelize,
    modelName: 'City',
  }
);

City.hasMany(User, { foreignKey: 'cityId' });
User.belongsTo(City, { foreignKey: 'cityId' });

export default City;
