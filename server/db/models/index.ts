import { Dialect, Sequelize } from 'sequelize';

interface Config {
  username: string;
  password: string;
  database: string;
  host?: string;
  dialect: Dialect;
  logging?: boolean | ((sql: string, timing?: number | undefined) => void);
  use_env_variable?: boolean;
}

const env = process.env.NODE_ENV || 'development';
const config: Config = require(__dirname + '/../config/config.json')[env];

let sequelize: Sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASS,
    {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      dialect: 'mysql',
      logging: process.env.DB_LOGGING,
    }
  );
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

export { sequelize, Sequelize };
