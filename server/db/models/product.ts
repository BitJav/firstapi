import { Model, Optional, DataTypes } from 'sequelize';
import { sequelize } from './index';

interface ProductAttributes {
  id: number;
  name?: string;
  price?: number;
  stock?: number;
}

interface ProductCreationAttributes extends Optional<ProductAttributes, 'id'> {}

class Product
  extends Model<ProductAttributes, ProductCreationAttributes>
  implements ProductAttributes
{
  declare id: number;
  declare name?: string;
  declare price?: number;
  declare stock?: number;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;
}

Product.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    price: DataTypes.DECIMAL(10, 2),
    stock: DataTypes.INTEGER,
  },
  {
    sequelize,
    modelName: 'Product',
  }
);

export default Product;
