import {
  Model,
  DataTypes,
  Optional,
  HasManyGetAssociationsMixin,
  HasManyAddAssociationMixin,
  HasManyHasAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  Association,
} from 'sequelize';
import { sequelize } from './index';
import User from './user';

export interface StateAttributes {
  id: number;
  name: string;
}

// Some attributes are optional in `State.build` and `State.create` calls
interface StateCreationAttributes extends Optional<StateAttributes, 'id'> {}

class State
  extends Model<StateAttributes, StateCreationAttributes>
  implements StateAttributes
{
  declare id: number;
  declare name: string;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;

  declare getCities: HasManyGetAssociationsMixin<State>;
  declare addState: HasManyAddAssociationMixin<State, number>;
  declare hasState: HasManyHasAssociationMixin<State, number>;
  declare countCities: HasManyCountAssociationsMixin;
  declare createState: HasManyCreateAssociationMixin<State>;

  declare static associations: {
    users: Association<State, User>;
  };
}

State.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
  },
  {
    sequelize,
    modelName: 'State',
  }
);

State.hasMany(User, { foreignKey: 'stateId' });
User.belongsTo(State, { foreignKey: 'stateId' });

export default State;
