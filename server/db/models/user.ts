import {
  Model,
  DataTypes,
  Optional,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  BelongsToCreateAssociationMixin,
  Association,
} from 'sequelize';
import { sequelize } from './index';
import City from './city';
import State from './state';

export interface UserAttributes {
  id: number;
  name: string;
  surname?: string;
  age?: number;
  address?: string;
  cityId: number;
  stateId: number;
}

// Some attributes are optional in `User.build` and `User.create` calls
export interface UserCreationAttributes
  extends Optional<UserAttributes, 'id'> {}

class User
  extends Model<UserAttributes, UserCreationAttributes>
  implements UserAttributes
{
  declare id: number;
  declare name: string;
  declare surname?: string;
  declare age?: number;
  declare address?: string;
  declare cityId: number;
  declare stateId: number;

  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;

  declare getCity: BelongsToGetAssociationMixin<City>;
  declare setCity: BelongsToSetAssociationMixin<City, number>;
  declare createCity: BelongsToCreateAssociationMixin<City>;

  declare getState: BelongsToGetAssociationMixin<State>;
  declare setState: BelongsToSetAssociationMixin<State, number>;
  declare createState: BelongsToCreateAssociationMixin<State>;

  declare static associations: {
    city: Association<User, City>;
    State: Association<User, State>;
  };
}

User.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
        len: [3, 50],
      },
    },
    surname: DataTypes.STRING,
    age: {
      type: DataTypes.INTEGER,
      validate: {
        min: 18,
      },
    },
    address: DataTypes.STRING,
    cityId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
    stateId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      validate: {
        notNull: true,
        notEmpty: true,
      },
    },
  },
  {
    sequelize,
    modelName: 'User',
  }
);

export default User;
