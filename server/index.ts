const dotenv = require('dotenv');

dotenv.config();

import Server from './server';
import UserRepository from './repositories/userRepository';
import ProductRepository from './repositories/productRepository';
import CityRepository from './repositories/cityRepository';
import StateRepository from './repositories/stateRepository';

const context = {
  apiContext: {
    userRepository: new UserRepository(),
    productRepository: new ProductRepository(),
    cityRepository: new CityRepository(),
    stateRepository: new StateRepository(),
  },
};

const server = new Server(context, 8080);

server.start();
