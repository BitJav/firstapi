import { sequelize } from '../db/models';
import City from '../db/models/city';

class CityRepository {
  findAll(options = {}) {
    return City.findAll(options);
  }

  create(city: City) {
    return City.create(city);
  }

  update(city: City, cityId: string) {
    return City.update(city, {
      where: { id: cityId },
    });
  }

  destroy(cityId: string) {
    return City.destroy({ where: { id: cityId } });
  }

  findById(cityId: string, options = {}) {
    return City.findByPk(cityId, options);
  }

  close() {
    sequelize.close();
  }
}

export default CityRepository;
