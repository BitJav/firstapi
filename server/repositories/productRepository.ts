import { sequelize } from '../db/models';
import Product from '../db/models/product';

class ProductRepository {
  findAll(options = {}) {
    return Product.findAll(options);
  }

  create(product: Product) {
    return Product.create(product);
  }

  update(product: Product, productId: string) {
    return Product.update(product, {
      where: { id: productId },
    });
  }

  destroy(productId: string) {
    return Product.destroy({ where: { id: productId } });
  }

  findById(productId: string) {
    return Product.findByPk(productId);
  }

  close() {
    sequelize.close();
  }
}

export default ProductRepository;
