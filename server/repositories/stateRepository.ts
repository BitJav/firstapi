import { sequelize } from '../db/models';
import State from '../db/models/state';

class StateRepository {
  findAll(options = {}) {
    return State.findAll(options);
  }

  create(state: State) {
    return State.create(state);
  }

  update(state: State, stateId: string) {
    return State.update(state, {
      where: { id: stateId },
    });
  }

  destroy(stateId: string) {
    return State.destroy({ where: { id: stateId } });
  }

  findById(stateId: string, options = {}) {
    return State.findByPk(stateId, options);
  }

  close() {
    sequelize.close();
  }
}

export default StateRepository;
