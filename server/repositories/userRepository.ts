import { sequelize } from '../db/models';
import User from '../db/models/user';

class UserRepository {
  findAll(options = {}) {
    return User.findAll(options);
  }

  create(user: User) {
    return User.create(user);
  }

  update(user: User, userId: string) {
    return User.update(user, {
      where: { id: userId },
    });
  }

  destroy(userId: string) {
    return User.destroy({ where: { id: userId } });
  }

  findById(userId: string, options = {}) {
    return User.findByPk(userId, options);
  }

  close() {
    sequelize.close();
  }
}

export default UserRepository;
