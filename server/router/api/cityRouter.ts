import express, { Router } from 'express';
import lodash from 'lodash';
import CityRepository from '../../repositories/cityRepository';

class CityRouter {
  private cityRepository: CityRepository;
  router: Router;

  constructor(cityRepository: CityRepository) {
    this.cityRepository = cityRepository;
    this.router = express.Router();
    this.loadRouter();
  }

  loadRouter() {
    this.router.get('/', (_, res) => {
      this.cityRepository
        .findAll()
        .then((cities) => {
          res.json(cities);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.get('/:cityId', (req, res) => {
      this.cityRepository
        .findById(req.params.cityId)
        .then((city) => {
          if (city) {
            res.json(city);
          } else {
            res.status(404).json({ failure: 'City not found' });
          }
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.post('/', (req, res) => {
      this.cityRepository
        .create(req.body)
        .then((city) => {
          res.status(201).json(city);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.put('/:cityId', (req, res) => {
      const city = req.body;
      const cityId = req.params.cityId;
      if (!lodash.isEmpty(city)) {
        this.cityRepository
          .update(city, req.params.cityId)
          .then((result) => {
            const [updatedQuantity, _] = result;
            if (updatedQuantity == 1) {
              res.json({ success: `City ${cityId} successfully updated` });
            } else {
              res.status(404).json({ failure: 'City not found' });
            }
          })
          .catch((err) => {
            res.status(500).json(err);
          });
      } else {
        res
          .status(400)
          .json({ failure: "Must provide json object with city's values" });
      }
    });

    this.router.delete('/:cityId', (req, res) => {
      this.cityRepository
        .destroy(req.params.cityId)
        .then((deletedQuantity) => {
          if (deletedQuantity == 1) {
            res.json({ success: 'City successfully deleted' });
          } else {
            res.status(404).json({ failure: 'City not found' });
          }
        })
        .catch((err) => {
          res.status(500).json(err);
        });
    });
  }
}

export default CityRouter;
