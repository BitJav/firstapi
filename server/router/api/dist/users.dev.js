"use strict";

var jsonPatch = require('fast-json-patch');

var express = require('express');

var router = express.Router();

var _require = require('../../db'),
    User = _require.User;

var _require2 = require('../../db'),
    City = _require2.City;

router.get('/', function _callee(req, res) {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          User.findAll({
            include: City
          }).then(function (users) {
            res.json(users);
          })["catch"](function (err) {
            res.json(err.toString());
          });

        case 1:
        case "end":
          return _context.stop();
      }
    }
  });
});
router.get('/:userId', function (req, res) {
  var userId = req.params.userId;
  User.findOne({
    include: City
  }) // User.findOne()
  .then(function (user) {
    // const a = user.getCity();
    res.json(a);
  })["catch"](function (err) {
    res.json(err.toString());
  });
});
router.post('/', function _callee2(req, res) {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          User.create(req.body).then(function (user) {
            res.json({
              success: 'User creado exitosamente'
            });
          })["catch"](function (err) {
            res.json(err.toString());
          });

        case 1:
        case "end":
          return _context2.stop();
      }
    }
  });
});
router.put('/:userId', function (req, res) {
  User.update(req.body, {
    where: {
      id: req.params.userId
    }
  }).then(function (user) {
    res.json({
      success: 'User actualizado exitosamente'
    });
  })["catch"](function (err) {
    res.json(err);
  });
});
router["delete"]('/:userId', function (req, res) {
  User.destroy({
    where: {
      id: req.params.userId
    }
  }).then(function (user) {
    res.json({
      success: 'borrado exitosamente'
    });
  })["catch"](function () {});
});
router.patch('/:userId', function (req, res) {
  var userId = req.params.userId;
  User.findByPk(userId).then(function (user) {
    var updatedUser = jsonPatch.applyPatch(user, req.body, true).newDocument;
    var updatedUserJson = JSON.parse(JSON.stringify(updatedUser));
    User.update(updatedUserJson, {
      where: {
        id: userId
      }
    }).then(function () {
      res.json({
        success: "User ".concat(userId, " actualizado exitosamente")
      });
    })["catch"](function (err) {
      res.json(err.toString());
    });
  })["catch"](function (err) {
    console.log('asdfadsf');
    res.json(err.toString());
  });
});
module.exports = router; // export default router;