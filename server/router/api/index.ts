import express, { Router } from 'express';
import { ApiContext } from '../../types/context';
import UserRouter from './userRouter';
import ProductRouter from './productRouter';
import CityRouter from './cityRouter';
import StateRouter from './stateRouter';

class ApiRouter {
  apiContext: ApiContext;
  router: Router;

  constructor(apiContext: ApiContext) {
    this.apiContext = apiContext;
    this.router = express.Router();
    this.loadRouter();
  }

  loadRouter() {
    const {
      userRepository,
      productRepository,
      cityRepository,
      stateRepository,
    } = this.apiContext;
    const userRouter = new UserRouter(userRepository);
    const productRouter = new ProductRouter(productRepository);
    const cityRouter = new CityRouter(cityRepository);
    const stateRouter = new StateRouter(stateRepository);
    const usersRouter = userRouter.router;
    const productsRouter = productRouter.router;
    const citiesRouter = cityRouter.router;
    const statesRouter = stateRouter.router;

    this.router.use('/users', usersRouter);
    this.router.use('/products', productsRouter);
    this.router.use('/cities', citiesRouter);
    this.router.use('/states', statesRouter);
  }
}

export default ApiRouter;
