import express, { Router } from 'express';
import lodash from 'lodash';
import ProductRepository from '../../repositories/productRepository';

class ProductRouter {
  private productRepository: ProductRepository;
  router: Router;

  constructor(productRepository: ProductRepository) {
    this.productRepository = productRepository;
    this.router = express.Router();
    this.loadRouter();
  }

  loadRouter() {
    this.router.get('/', (_, res) => {
      this.productRepository
        .findAll()
        .then((products) => {
          res.json(products);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.get('/:productId', (req, res) => {
      this.productRepository
        .findById(req.params.productId)
        .then((product) => {
          if (product) {
            res.json(product);
          } else {
            res.status(404).json({ failure: 'Product not found' });
          }
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.post('/', (req, res) => {
      this.productRepository
        .create(req.body)
        .then((product) => {
          res.status(201).json(product);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.put('/:productId', (req, res) => {
      const product = req.body;
      const productId = req.params.productId;
      if (!lodash.isEmpty(product)) {
        this.productRepository
          .update(product, productId)
          .then((result) => {
            const [updatedQuantity, _] = result;
            if (updatedQuantity == 1) {
              res.json({
                success: `Product ${productId} successfully updated`,
              });
            } else {
              res.status(404).json({ failure: 'Product not found' });
            }
          })
          .catch((err) => {
            res.json(err.toString());
          });
      } else {
        res
          .status(400)
          .json({ failure: "Must provide json object with product's values" });
      }
    });

    this.router.delete('/:productId', (req, res) => {
      const productId = req.params.productId;
      this.productRepository
        .destroy(productId)
        .then((deletedQuantity) => {
          if (deletedQuantity == 1) {
            res.json({ success: `Product ${productId} successfully deleted` });
          } else {
            res.status(404).json({ failure: 'Product not found' });
          }
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });
  }
}

export default ProductRouter;
