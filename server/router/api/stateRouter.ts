import express, { Router } from 'express';
import lodash from 'lodash';
import StateRepository from '../../repositories/stateRepository';

class StateRouter {
  private stateRepository: StateRepository;
  router: Router;

  constructor(stateRepository: StateRepository) {
    this.stateRepository = stateRepository;
    this.router = express.Router();
    this.loadRouter();
  }

  loadRouter() {
    this.router.get('/', (_, res) => {
      this.stateRepository
        .findAll({ include: 'Users' })
        .then((states) => {
          res.json(states);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.get('/:stateId', (req, res) => {
      const stateId = req.params.stateId;
      this.stateRepository
        .findById(stateId, { include: 'Users' })
        .then((state) => {
          if (state) {
            res.json(state);
          } else {
            res.status(404).json({ failure: 'State not found' });
          }
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.post('/', (req, res) => {
      this.stateRepository
        .create(req.body)
        .then((state) => {
          res.status(201).json(state);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.put('/:stateId', (req, res) => {
      const state = req.body;
      const stateId = req.params.stateId;
      if (!lodash.isEmpty(state)) {
        this.stateRepository
          .update(state, stateId)
          .then((result) => {
            const [updatedQuantity, _] = result;
            if (updatedQuantity == 1) {
              res.json({ success: `State ${stateId} successfully updated` });
            } else {
              res.status(404).json({ failure: 'State not found' });
            }
          })
          .catch((err) => {
            res.status(500).json(err.toString());
          });
      } else {
        res
          .status(400)
          .json({ failure: "Must provide json object with state's values" });
      }
    });

    this.router.delete('/:stateId', (req, res) => {
      this.stateRepository
        .destroy(req.params.stateId)
        .then((deletedQuantity) => {
          if (deletedQuantity == 1) {
            res.json({ success: 'State successfully deleted' });
          } else {
            res.status(404).json({ failure: 'State not found' });
          }
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });
  }
}

export default StateRouter;
