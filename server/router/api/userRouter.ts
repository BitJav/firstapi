import express, { Router } from 'express';
import lodash from 'lodash';
import UserRepository from '../../repositories/userRepository';

const USER_NOT_FOUND = 'User not found';

class UserRouter {
  private userRepository: UserRepository;
  router: Router;

  constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
    this.router = express.Router();
    this.loadRouter();
  }

  loadRouter() {
    this.router.get('/', (_, res) => {
      this.userRepository
        .findAll({ include: ['City', 'State'] })
        .then((users) => {
          res.json(users);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.get('/:userId', (req, res) => {
      const userId = req.params.userId;
      this.userRepository
        .findById(userId, { include: ['City', 'State'] })
        .then((user) => {
          if (user) {
            res.json(user);
          } else {
            res.status(404).json({ failure: USER_NOT_FOUND });
          }
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.post('/', (req, res) => {
      this.userRepository
        .create(req.body)
        .then((user) => {
          res.status(201).json(user);
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });

    this.router.put('/:userId', (req, res) => {
      const user = req.body;
      const userId = req.params.userId;
      if (!lodash.isEmpty(user)) {
        this.userRepository
          .update(user, userId)
          .then((result) => {
            const [updatedQuantity, _] = result;
            if (updatedQuantity == 1) {
              res.json({ success: `User ${userId} successfully updated` });
            } else {
              res.status(404).json({ failure: USER_NOT_FOUND });
            }
          })
          .catch((err) => {
            res.status(500).json(err.toString());
          });
      } else {
        res
          .status(400)
          .json({ failure: "Must provide json object with user's values" });
      }
    });

    this.router.delete('/:userId', (req, res) => {
      this.userRepository
        .destroy(req.params.userId)
        .then((deletedQuantity) => {
          if (deletedQuantity == 1) {
            res.json({ success: 'User successfully deleted' });
          } else {
            res.status(404).json({ failure: USER_NOT_FOUND });
          }
        })
        .catch((err) => {
          res.status(500).json(err.toString());
        });
    });
  }
}

export default UserRouter;
