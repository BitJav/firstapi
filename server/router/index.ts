import express, { Router as expRouter } from 'express';
import { Context } from '../types/context';
import ApiRouter from './api';

class Router {
  context: Context;
  router: expRouter;

  constructor(context: Context) {
    this.context = context;
    this.router = express.Router();
    this.loadRouter();
  }

  loadRouter() {
    const apiRouter = new ApiRouter(this.context.apiContext);
    this.router.use('/api', apiRouter.router);
  }
}

export default Router;
