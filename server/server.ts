import express, { Application } from 'express';
import * as http from 'http';
import Router from './router';
import { Context } from './types/context';

class Server {
  context: Context;
  port: number;
  app: Application;
  // @ts-ignore
  startedServer: http.Server;

  constructor(context: Context, port = 8080) {
    this.context = context;
    this.port = port;
    this.app = express();
    this.init();
  }

  init() {
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(express.json());
    const router = new Router(this.context);
    this.app.use('/', router.router);
  }

  start() {
    this.startedServer = this.app.listen(this.port, () => {
      console.log(
        'App iniciada exitosamente, escuchando en puerto ' + this.port
      );
    });
  }

  stop() {
    this.startedServer.close();
  }
}

export default Server;
