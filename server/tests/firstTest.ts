import assert from 'assert';

describe('Top level description', function a() {
  describe('middle level desc', function b() {
    it('Two string literals should be equal', function c() {
      assert.equal('aaaa', 'aaaa');
    });
    it.skip('Two terals should be equal', function c() {
      assert.equal('aaa', 'aaaa');
    });
    it.skip('Two  be equal', function c() {
      assert.equal('aa', 'aaaa');
    });
  });

  describe('Second middle suite', function b() {
    it('Two string literals should be equal', function c() {
      assert.equal('aaaa', 'aaaa');
    });
    it('Two terals should be equal', function c() {
      assert.equal('aaaa', 'aaaa');
    });
    it('Two  be equal', function c() {
      assert.equal('aaaa', 'aaaa');
    });
  });
});
