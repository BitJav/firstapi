import faker from 'faker';
import * as chai from 'chai';
import request, { Response } from 'supertest';
import chaiHTTP from 'chai-http';
import Server from '../../server';
import CityRepository from '../../repositories/cityRepository';
import fakeData from './fakeData';

import { Context } from '../../types/context';

chai.use(chaiHTTP);

// Server setup:
// - A regular server to test error paths

const cityRepository = new CityRepository();

let context: Context = {
  apiContext: {
    userRepository: undefined,
    productRepository: undefined,
    cityRepository: cityRepository,
    stateRepository: undefined,
  },
};

const server = new Server(context, 8080);
// --------------------------------------------------------
const expect = chai.expect;
const url = '/api';
const FAKE_CITY_ID = 4;

describe('City IT Tests', () => {
  describe('GET', () => {
    describe('Success tests', function () {
      it('Cities should retrieve an array', (done) => {
        request(server.app)
          .get(url + '/cities')
          .end((err: any, res: Response) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('array');
            expect(res.body).to.have.lengthOf(fakeData.city.data.length);
            done();
          });
      });
    });

    it('Getting a city by ID should retrieve only one element', (done) => {
      request(server.app)
        .get(url + '/cities/1')
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.an('object');
          done();
        });
    });

    describe('Failure tests', function () {
      it('Getting a city with negative ID should show error', (done) => {
        request(server.app)
          .get(url + '/cities/-10')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('City not found');
            done();
          });
      });
    });
  });

  describe('POST', function () {
    describe('Success tests', function () {
      it('Should create a city correctly', (done) => {
        const city = {
          name: faker.name.findName(),
        };

        request(server.app)
          .post(url + '/cities')
          .send(city)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(201);
            expect(res.body).to.include(city);
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Create city without body should show 'not null' message", (done) => {
        request(server.app)
          .post(url + '/cities')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('City.name cannot be null');
            done();
          });
      });

      it('Create city with empty name should show validation error message', (done) => {
        const city = { name: '' };
        request(server.app)
          .post(url + '/cities')
          .send(city)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation notEmpty on name failed');
            done();
          });
      });
    });
  });

  describe('PUT', function () {
    describe('Success tests', function () {
      it('Should update a city correctly', (done) => {
        const city = {
          name: faker.name.findName(),
        };

        request(server.app)
          .put(url + '/cities/' + FAKE_CITY_ID)
          .send(city)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include(
              `City ${FAKE_CITY_ID} successfully updated`
            );
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Update city without body should show 'empty body not permitted' message", (done) => {
        request(server.app)
          .put(url + '/cities/' + FAKE_CITY_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(400);
            expect(res.text).to.include(
              "Must provide json object with city's values"
            );
            done();
          });
      });

      it('Update city with empty name should show validation error message', (done) => {
        const city = { name: '' };
        request(server.app)
          .put(url + '/cities/' + FAKE_CITY_ID)
          .send(city)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation notEmpty on name failed');
            done();
          });
      });
    });
  });

  describe('DELETE', () => {
    describe('Success tests', function () {
      it('Deletes successfully a city', (done) => {
        request(server.app)
          .delete(url + '/cities/' + FAKE_CITY_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include('City successfully deleted');
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Deleting non-existing city should show 'non-existing city' message", (done) => {
        request(server.app)
          .delete(url + '/cities/' + -5)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('City not found');
            done();
          });
      });
    });
  });
});
