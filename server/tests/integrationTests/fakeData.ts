import faker from 'faker';
import CityRepository from '../../repositories/cityRepository';
import StateRepository from '../../repositories/stateRepository';
import UserRepository from '../../repositories/userRepository';
import ProductRepository from '../../repositories/productRepository';

interface FakeData {
  [model: string]: {
    repository:
      | CityRepository
      | StateRepository
      | UserRepository
      | ProductRepository;
    data: any[];
  };
}

const fakeData: FakeData = {
  city: {
    repository: new CityRepository(),
    data: [
      { name: faker.address.city() },
      { name: faker.address.city() },
      { name: faker.address.city() },
      { name: faker.address.city() },
      { name: faker.address.city() },
    ],
  },
  state: {
    repository: new StateRepository(),
    data: [
      { name: faker.address.state() },
      { name: faker.address.state() },
      { name: faker.address.state() },
      { name: faker.address.state() },
      { name: faker.address.state() },
    ],
  },
  user: {
    repository: new UserRepository(),
    data: [
      {
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        address: 'Calle falsa 123',
        age: 20,
        cityId: 1,
        stateId: 1,
      },
      {
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        age: 18,
        address: 'Calle falsa 234',
        cityId: 1,
        stateId: 1,
      },
      {
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        age: 25,
        address: 'Calle falsa 345',
        cityId: 1,
        stateId: 1,
      },
      {
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        age: 40,
        address: 'Calle falsa 456',
        cityId: 1,
        stateId: 1,
      },
      {
        name: faker.name.firstName(),
        surname: faker.name.lastName(),
        age: 56,
        address: 'Calle falsa 567',
        cityId: 1,
        stateId: 1,
      },
    ],
  },
  product: {
    repository: new ProductRepository(),
    data: [
      {
        name: faker.commerce.product(),
        price: faker.commerce.price(),
        stock: faker.datatype.number(20),
      },
      {
        name: faker.commerce.product(),
        price: faker.commerce.price(),
        stock: faker.datatype.number(20),
      },
      {
        name: faker.commerce.product(),
        price: faker.commerce.price(),
        stock: faker.datatype.number(20),
      },
      {
        name: faker.commerce.product(),
        price: faker.commerce.price(),
        stock: faker.datatype.number(20),
      },
      {
        name: faker.commerce.product(),
        price: faker.commerce.price(),
        stock: faker.datatype.number(20),
      },
    ],
  },
};

export default fakeData;
