import { sequelize } from '../../db/models';
import fakeData from './fakeData';

sequelize
  .sync({ force: true })
  .then((_: any) => {
    for (let model in fakeData) {
      console.log('Loading data for ' + model);
      const repository = fakeData[model].repository;
      const data = fakeData[model].data;
      data.forEach((object) => {
        if (model == 'user') {
          repository.create(object).catch((err) => {
            if (err.toString().includes('Validation len on name failed')) {
              object.name = 'aaaa';
              repository.create(object);
            }
          });
        } else {
          repository.create(object);
        }
      });
    }
  })
  .catch((err) => {
    console.log(err.toString());
  });
