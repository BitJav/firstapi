import faker from 'faker';
import chai from 'chai';
import request from 'supertest';
import chaiHTTP from 'chai-http';
import Server from '../../server';
import ProductRepository from '../../repositories/productRepository';
import fakeData from './fakeData';
import { Context } from '../../types/context';

chai.use(chaiHTTP);

// Server setup:
const productRepository = new ProductRepository();

let context: Context = {
  apiContext: {
    userRepository: undefined,
    productRepository: productRepository,
    cityRepository: undefined,
    stateRepository: undefined,
  },
};

const server = new Server(context, 8080);
// --------------------------------------------------------

const expect = chai.expect;
const url = '/api';
const PRODUCT_ID = 4;

describe('Product IT tests', () => {
  describe('GET', () => {
    describe('Success tests', function () {
      it('Products should retrieve an array', (done) => {
        request(server.app)
          .get(url + '/products')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('array');
            expect(res.body).to.have.lengthOf(fakeData.product.data.length);
            done();
          });
      });

      it('Getting a product by ID should retrieve only one element', (done) => {
        request(server.app)
          .get(url + '/products/1')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('object');
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it('Getting a product with negative ID should show error', (done) => {
        request(server.app)
          .get(url + '/products/-10')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('Product not found');
            done();
          });
      });
    });
  });

  describe('POST', function () {
    describe('Success tests', function () {
      it('Should create a product correctly', (done) => {
        const product = {
          name: faker.commerce.product(),
          price: faker.commerce.price(),
          stock: faker.datatype.number(20),
        };

        request(server.app)
          .post(url + '/products')
          .send(product)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(201);
            expect(res.body).to.include(product);
            done();
          });
      });
    });

    describe('Failure tests', function () {});
  });

  describe('PUT', function () {
    describe('Success tests', function () {
      it('Should update a product correctly', (done) => {
        const product = {
          name: faker.name.findName(),
        };

        request(server.app)
          .put(url + '/products/' + PRODUCT_ID)
          .send(product)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include(
              `Product ${PRODUCT_ID} successfully updated`
            );
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Update product without body should show 'empty body not permitted' message", (done) => {
        request(server.app)
          .put(url + '/products/' + PRODUCT_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(400);
            expect(res.text).to.include(
              "Must provide json object with product's values"
            );
            done();
          });
      });
    });
  });

  describe('DELETE', () => {
    describe('Success tests', function () {
      it('Deletes successfully a product', (done) => {
        request(server.app)
          .delete(url + '/products/' + PRODUCT_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include(
              `Product ${PRODUCT_ID} successfully deleted`
            );
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Deleting non-existing product should show 'non-existing product' message", (done) => {
        request(server.app)
          .delete(url + '/products/' + -5)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('Product not found');
            done();
          });
      });
    });
  });
});
