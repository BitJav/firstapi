import faker from 'faker';
import chai from 'chai';
import request from 'supertest';
import chaiHTTP from 'chai-http';
import Server from '../../server';
import StateRepository from '../../repositories/stateRepository';
import fakeData from './fakeData';
import { Context } from '../../types/context';

chai.use(chaiHTTP);

// Servers setup:
// - A regular server to test error paths

const stateRepository = new StateRepository();

let context: Context = {
  apiContext: {
    userRepository: undefined,
    productRepository: undefined,
    cityRepository: undefined,
    stateRepository: stateRepository,
  },
};

const server = new Server(context, 8080);
// --------------------------------------------------------

const expect = chai.expect;
const url = '/api';
const FAKE_STATE_ID = 4;

describe('State IT Tests', () => {
  describe('GET', () => {
    describe('Success tests', function () {
      it('States should retrieve an array', (done) => {
        request(server.app)
          .get(url + '/states')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('array');
            expect(res.body).to.have.lengthOf(fakeData.state.data.length);
            done();
          });
      });
    });

    it('Getting a state by ID should retrieve only one element', (done) => {
      chai
        .request(server.app)
        .get(url + '/states/1')
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.an('object');
          done();
        });
    });

    describe('Failure tests', function () {
      it('Getting a state with negative ID should show error', (done) => {
        request(server.app)
          .get(url + '/states/-10')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('State not found');
            done();
          });
      });
    });
  });

  describe('POST', function () {
    describe('Success tests', function () {
      it('Should create a state correctly', (done) => {
        const state = {
          name: faker.name.findName(),
        };

        request(server.app)
          .post(url + '/states')
          .send(state)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(201);
            expect(res.body).to.include(state);
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Create state without body should show 'not null' message", (done) => {
        request(server.app)
          .post(url + '/states')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('State.name cannot be null');
            done();
          });
      });

      it('Create state with empty name should show validation error message', (done) => {
        const state = { name: '' };
        request(server.app)
          .post(url + '/states')
          .send(state)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation notEmpty on name failed');
            done();
          });
      });
    });
  });

  describe('PUT', function () {
    describe('Success tests', function () {
      it('Should update a state correctly', (done) => {
        const state = {
          name: faker.name.findName(),
        };

        request(server.app)
          .put(url + '/states/' + FAKE_STATE_ID)
          .send(state)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include(
              `State ${FAKE_STATE_ID} successfully updated`
            );
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Update state without body should show 'empty body not permitted' message", (done) => {
        request(server.app)
          .put(url + '/states/' + FAKE_STATE_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(400);
            expect(res.text).to.include(
              "Must provide json object with state's values"
            );
            done();
          });
      });

      it('Update state with empty name should show validation error message', (done) => {
        const state = { name: '' };
        request(server.app)
          .put(url + '/states/' + FAKE_STATE_ID)
          .send(state)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation notEmpty on name failed');
            done();
          });
      });
    });
  });

  describe('DELETE', () => {
    describe('Success tests', function () {
      it('Deletes successfully a state', (done) => {
        request(server.app)
          .delete(url + '/states/' + FAKE_STATE_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include('State successfully deleted');
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Deleting non-existing state should show 'non-existing state' message", (done) => {
        request(server.app)
          .delete(url + '/states/' + -5)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('State not found');
            done();
          });
      });
    });
  });
});
