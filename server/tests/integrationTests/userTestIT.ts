import faker from 'faker';
import chai from 'chai';
import request from 'supertest';
import chaiHTTP from 'chai-http';
import Server from '../../server';
import UserRepository from '../../repositories/userRepository';
import fakeData from './fakeData';
import { Context } from '../../types/context';

chai.use(chaiHTTP);

// Server setup:
const userRepository = new UserRepository();

let context: Context = {
  apiContext: {
    userRepository: userRepository,
    productRepository: undefined,
    cityRepository: undefined,
    stateRepository: undefined,
  },
};

const server = new Server(context, 8080);
// --------------------------------------------------------

const expect = chai.expect;
const url = '/api';
const USER_ID = 4;
const validationMessages = [
  'User.name cannot be null',
  'User.cityId cannot be null',
  'User.stateId cannot be null',
];

describe('User IT tests', () => {
  describe('GET', () => {
    describe('Success tests', function () {
      it('Users should retrieve an array', (done) => {
        request(server.app)
          .get(url + '/users')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('array');
            expect(res.body).to.have.lengthOf(fakeData.user.data.length);
            done();
          });
      });

      it('Getting a user by ID should retrieve only one element', (done) => {
        request(server.app)
          .get(url + '/users/1')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.body).to.be.an('object');
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it('Getting a user with negative ID should show error', (done) => {
        request(server.app)
          .get(url + '/users/-10')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('User not found');
            done();
          });
      });
    });
  });

  describe('POST', function () {
    describe('Success tests', function () {
      it('Should create a user correctly', (done) => {
        const user = {
          name: faker.name.firstName(),
          surname: faker.name.lastName(),
          age: 20,
          cityId: 1,
          stateId: 1,
        };

        request(server.app)
          .post(url + '/users')
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(201);
            expect(res.body).to.include(user);
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Create user without body should show 'not null' message", (done) => {
        request(server.app)
          .post(url + '/users')
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('User.name cannot be null');
            done();
          });
      });

      it('Create user with empty name should show validation error message', (done) => {
        const user = { name: '' };
        request(server.app)
          .post(url + '/users')
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation notEmpty on name failed');
            done();
          });
      });

      it('Create empty user should show validation error messages', (done) => {
        const user = {};
        request(server.app)
          .post(url + '/users')
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            for (const validationMessage of validationMessages) {
              expect(res.text).to.include(validationMessage);
            }
            done();
          });
      });

      it('Create user with null values should show validation error messages', (done) => {
        const user = {
          name: null,
          cityId: null,
          stateId: null,
        };
        request(server.app)
          .post(url + '/users')
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            for (const validationMessage of validationMessages) {
              expect(res.text).to.include(validationMessage);
            }
            done();
          });
      });

      it('Create user with short name should fail', (done) => {
        const user = {
          name: 'aa',
          cityId: 1,
          stateId: 2,
        };
        request(server.app)
          .post(url + '/users')
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation len on name failed');
            done();
          });
      });

      it('Create user with age lower than 18 should fail', (done) => {
        const user = {
          name: 'josh',
          age: 17,
          cityId: 1,
          stateId: 2,
        };
        request(server.app)
          .post(url + '/users')
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation min on age failed');
            done();
          });
      });
    });
  });

  describe('PUT', function () {
    describe('Success tests', function () {
      it('Should update a user correctly', (done) => {
        const user = {
          name: faker.name.findName(),
        };

        request(server.app)
          .put(url + '/users/' + USER_ID)
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include(`User ${USER_ID} successfully updated`);
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Update user without body should show 'empty body not permitted' message", (done) => {
        request(server.app)
          .put(url + '/users/' + USER_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(400);
            expect(res.text).to.include(
              "Must provide json object with user's values"
            );
            done();
          });
      });

      it('Update user with empty name should show validation error message', (done) => {
        const user = { name: '' };
        request(server.app)
          .put(url + '/users/' + USER_ID)
          .send(user)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(500);
            expect(res.text).to.include('Validation notEmpty on name failed');
            done();
          });
      });
    });
  });

  describe('DELETE', () => {
    describe('Success tests', function () {
      it('Deletes successfully a user', (done) => {
        request(server.app)
          .delete(url + '/users/' + USER_ID)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            expect(res.text).to.include('User successfully deleted');
            done();
          });
      });
    });

    describe('Failure tests', function () {
      it("Deleting non-existing user should show 'non-existing user' message", (done) => {
        request(server.app)
          .delete(url + '/users/' + -5)
          .end((err, res) => {
            expect(err).to.be.null;
            expect(res).to.have.status(404);
            expect(res.text).to.include('User not found');
            done();
          });
      });
    });
  });
});
