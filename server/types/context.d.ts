import CityRepository from './repositories/cityRepository';
import ProductRepository from './repositories/productRepository';
import StateRepository from './repositories/stateRepository';
import UserRepository from './repositories/userRepository';

export interface ApiContext {
  userRepository: UserRepository;
  productRepository: ProductRepository;
  cityRepository: CityRepository;
  stateRepository: StateRepository;
}

export interface Context {
  apiContext: ApiContext;
}
